#version 150

uniform vec4 AmbientProduct;
uniform vec4 DiffuseProduct;
uniform vec4 SpecularProduct;
uniform vec4 Light;
uniform float Shininess;

in vec4 pos;
in vec4 N;
in vec2 texCoord;

uniform sampler2D textureMain;

out vec4 fragColor;

void main()
{
  
  // Ambient
  vec4 ambient = AmbientProduct;
  
  // Diffuse
  vec4 diffuse_color = texture(textureMain, texCoord);
  //vec4 diffuse_color = vec4(texCoord, 0,1);
  float D  = 1.0;
  vec4  diffuse = D * diffuse_color;
  
  //Specular
  float S = 0.0;
  vec4  specular = S * SpecularProduct;
  
  fragColor = ambient + diffuse + specular;
}

